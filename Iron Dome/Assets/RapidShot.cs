﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RapidShot : MonoBehaviour
{
    [SerializeField] float initVelocity = 10f;
    [SerializeField] float spreadAngle = 20f;
    // Start is called before the first frame update
    void Start()
    {
        System.Random random = new System.Random();
        float offset = (float)(random.NextDouble() - 0.5) * 2 * spreadAngle;
        transform.rotation *= Quaternion.Euler(0, 0, offset);
        GetComponent<Rigidbody2D>().velocity = initVelocity * transform.up;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.layer == 12)
        {
            Destroy(gameObject);
        }
    }
}
