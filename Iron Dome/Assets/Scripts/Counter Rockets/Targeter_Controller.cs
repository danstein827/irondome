﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Targeter_Controller : MonoBehaviour
{

    [SerializeField] GameObject dropTarget;
    InputManager input;

    private bool moved = false;
    public bool Moved { get { return moved; }
        private set { moved = value; } }

    // Start is called before the first frame update
    void Start()
    {
        input = FindObjectOfType<InputManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if(input.Build == GameBuild.Windows)
        {
            mouseMove();
        }
    }

    private void mouseMove()
    {
        float x_move = input.x_Axis;
        float y_move = input.y_Axis;
        Moved = false;

        if (Mathf.Abs(x_move) > Mathf.Epsilon || Mathf.Abs(y_move) > Mathf.Epsilon)
        {
            Moved = true;
            Vector3 pos = Camera.main.WorldToViewportPoint(transform.position + new Vector3(x_move, y_move) * Time.deltaTime);
            pos.x = Mathf.Clamp01(pos.x);
            pos.y = Mathf.Clamp01(pos.y);

            transform.position = Camera.main.ViewportToWorldPoint(pos);
        }
    }
}
