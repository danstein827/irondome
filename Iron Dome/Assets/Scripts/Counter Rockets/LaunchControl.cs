﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchControl : MonoBehaviour
{
    GameObject rocketTarget;

    public Munition munition = Munition.standard;

    List<Rocket_Launcher> launchers;
    private Targeter_Controller target;
    public Targeter_Controller Target { get => target; }
    
    InputManager input;

    private int launcherIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        target = FindObjectOfType<Targeter_Controller>();
        launchers = new List<Rocket_Launcher>(GetComponentsInChildren<Rocket_Launcher>());
        launchers[launcherIndex].Order = LauncherOrder.loaded;
        input = FindObjectOfType<InputManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if(target.Moved)
        {
            launchers.ForEach(l =>
            {
                l.pointAtTargeter(target.gameObject.transform.position);
            });
        }

        if(input.fireCommand) { fire(); }
    }

    public void fire()
    {
        foreach(Rocket_Launcher l in launchers)
        {
            if(l.fire())
            {
                l.Order = LauncherOrder.queued;
                launcherIndex++;
                if(launcherIndex >= launchers.Count) { launcherIndex = 0; }
                launchers[launcherIndex].Order = LauncherOrder.loaded;
                break;
            }
        }
    }
}
