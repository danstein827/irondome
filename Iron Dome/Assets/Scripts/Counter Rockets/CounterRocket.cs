﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CounterRocket : MonoBehaviour
{
    [SerializeField] float initVel = 1;
    [SerializeField] GameObject explosion;
    Rigidbody2D rigidbody;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        rigidbody.velocity = initVel * transform.up;
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 12)
        { Destroy(this.gameObject); }
        else if(collision.gameObject.layer == 14)
        {
            Destroy(collision.gameObject);
            Explode();
        } else if(collision.gameObject.layer == 15)
        {
            Explode();

        }


    }

    private void Explode()
    {
        GameObject ex = Instantiate(explosion, transform.parent);
        ex.transform.position = transform.position;
        Destroy(this.gameObject);
    }

    public void setTimer(Vector3 targetPos)
    {
        Vector2 targetRel = targetPos - transform.position;
        float dist = targetRel.magnitude;
        float timer = dist / initVel;
        Invoke("Explode", timer);
    }
}
