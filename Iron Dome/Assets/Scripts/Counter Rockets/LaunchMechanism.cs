﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Munition
{
    standard = 0,
    fullAuto = 1
}

public class LaunchMechanism : MonoBehaviour
{
    [SerializeField] List<GameObject> munitions = new List<GameObject>();
    [SerializeField] float[] reloadTimes = { 5.0f, 2.0f };
    LaunchControl launchControl;
    int multishot = 0;

    // Start is called before the first frame update
    void Start()
    {
        launchControl = FindObjectOfType<LaunchControl>();
    }

    public float fire()
    {
        switch (launchControl.munition)
        {
            case (Munition.standard):
                return fireStandard();
            case (Munition.fullAuto):
                return fireFullAuto();
        }

        return 0f;
    }

    private float fireFullAuto()
    {
        multishot = 3;
        fullAuto();
        return reloadTimes[1];
    }

    private void fullAuto()
    {
        if(multishot > 0)
        {
            GameObject rapid = Instantiate(munitions[1], launchControl.transform);
            rapid.transform.position = transform.position;
            rapid.transform.rotation = transform.rotation;
            multishot--;
            Invoke("fullAuto", 0.1f);
        }
        
    }

    private float fireStandard()
    {
        GameObject rocket = Instantiate(munitions[0], launchControl.transform);
        rocket.transform.position = transform.position;
        rocket.transform.rotation = transform.rotation;
        rocket.GetComponent<CounterRocket>().setTimer(launchControl.Target.gameObject.transform.position);
        return reloadTimes[0];
    }
}
