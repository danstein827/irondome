﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket_Launcher : MonoBehaviour
{

    [SerializeField] float maxAngle = 90;
    [SerializeField] GameObject munition;   //Loaded Munition
    [SerializeField] float rocketOffset = 0.5f;
    [SerializeField] GameObject light;
    [SerializeField] List<Color> lightColors = new List<Color>{Color.green, Color.yellow, Color.red};

    LaunchControl launchControl;    //Parent
    LaunchMechanism mechanism;

    private LauncherStatus status;
    private LauncherOrder order;

    public LauncherStatus Status { get => status;
        set {
            status = value;
            setLightColor();
        }
    }

    public LauncherOrder Order { get => order;
        set
        {
            order = value;
            setLightColor();
        }
    }

    private void setLightColor()
    {
        SpriteRenderer sr = light.GetComponent<SpriteRenderer>();
        if(Status == LauncherStatus.notReady)
        {
            sr.color = lightColors[2];
        }
        else if(Status == LauncherStatus.ready && Order == LauncherOrder.queued)
        {
            sr.color = lightColors[1];
        }
        else if(Order == LauncherOrder.loaded)
        {
            sr.color = lightColors[0];
        }
    }



    // Start is called before the first frame update
    void Start()
    {
        launchControl = FindObjectOfType<LaunchControl>();
        mechanism = GetComponentInChildren<LaunchMechanism>();
        Status = LauncherStatus.ready;
        Order = LauncherOrder.queued;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void pointAtTargeter(Vector2 target)
    {
        Vector2 relative = target - new Vector2(transform.position.x, transform.position.y);
        float angle = Mathf.Rad2Deg*Mathf.Atan2(-relative.x, relative.y);
        float angle_Clamped = Mathf.Clamp(angle, -maxAngle, maxAngle);
        transform.rotation = Quaternion.Euler(0f, 0f, angle_Clamped);
    }

    public bool fire()
    {
        if(Order == LauncherOrder.loaded && Status == LauncherStatus.ready)
        {
            Status = LauncherStatus.notReady;
            Invoke("ReadyUp", mechanism.fire());
            return true;
        }
        return false;
    }

    private void ReadyUp()
    {
        Status = LauncherStatus.ready;
    }
}

public enum LauncherStatus { ready, notReady };
public enum LauncherOrder { loaded, queued };
