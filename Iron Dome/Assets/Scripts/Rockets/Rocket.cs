﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : MonoBehaviour
{
    [SerializeField] GameObject explosion;

    [SerializeField] float exStartSize = 0.05f;
    [SerializeField] float exEndSize = 0.25f;
    [SerializeField] float exLife = 0.5f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("Rocket hit");
        try
        {
            collision.gameObject.GetComponent<Building>().buildingHit();
        }
        catch { }

        explode();
        
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.layer == 15)
        {
            explode();
        }
    }

    private void explode()
    {
        GameObject ex = Instantiate(explosion, transform.parent);
        ex.GetComponent<Explosion>().startSize = exStartSize;
        ex.GetComponent<Explosion>().endSize = exEndSize;
        ex.GetComponent<Explosion>().lifeTime = exLife;
        ex.transform.position = transform.position;
        Destroy(gameObject);
    }
}
