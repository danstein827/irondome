﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DataLogger;

public class Rocket_Targeting : MonoBehaviour
{
    //Rocket's Target
    [SerializeField] GameObject target;
    [SerializeField] float angleToTarget;

    Rigidbody2D rigidbody2D;

    [SerializeField] float[] pidTune = new float[3] { 7, 1, 2 };

    PID_Controller pid_Controller;

    //Data Logging the angle
    [SerializeField] bool log_angle = true;
    [SerializeField] string file_name = "Data.csv";
    CSV_writer csv;
    CSV_Row row = new CSV_Row();

    [SerializeField] bool debugRays = true;

    // Start is called before the first frame update
    void Start()
    {
        pid_Controller = new PID_Controller(pidTune[0], pidTune[1], pidTune[2]);

        if(log_angle)
        {
            csv = new CSV_writer(file_name);
            write_CSV_Header();
        }
        ;

        rigidbody2D = GetComponent<Rigidbody2D>();
        target = FindObjectOfType<City_Script>().return_Target().gameObject;

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        writeAngleData();

        Vector2 toTarget = getToTargetVector();
        float angle = get_target_angle(toTarget);
        this.angleToTarget = Mathf.Rad2Deg * angle;
        rigidbody2D.AddTorque(pid_Controller.eval(angle));
    }

    private Vector2 getToTargetVector()
    {
        Vector2 toTarget = Vector2.zero;
        try
        {
            toTarget = target.transform.position - transform.position;
        }
        catch
        {
            target = FindObjectOfType<City_Script>().return_Target().gameObject;
        }

        return toTarget;
    }

    private void write_CSV_Header()
    {
        if (log_angle)
        {
            row.rowAppend(new List<string> { "Time", "Angle To Target", "Angle", "Angle Error" });
            csv.writeHeader(row);
        }
    }

    private void writeAngleData()
    {
        if(log_angle)
        {
            Vector2 rel_Vec = getToTargetVector();
            float angle = Mathf.Atan2(-rel_Vec.x, rel_Vec.y);
            row.rowAppend(new List<float> { Time.time, angle, transform.rotation.eulerAngles.z, this.angleToTarget });
            csv.writeLine(row);
        }
    }

    private float get_target_angle(Vector2 to_target)
    {
        to_target = to_target.normalized;
        float angle1 = Mathf.Asin(Vector3.Cross(transform.up, to_target).z);
        float angle2 = Mathf.Asin(Vector3.Cross(-transform.right, to_target).z);

        if (angle1 > 0.0f)
        {
            angle1 = (angle2 > 0.0f) ? Mathf.PI - angle1 : angle1;
        }
        else
        {
            angle1 = (angle2 > 0.0f) ? -Mathf.PI - angle1 : angle1;
        }

        return angle1;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, 10*transform.up);

        if(debugRays)
        {
            Gizmos.color = Color.white;
            Vector2 to_target = 10 * (target.transform.position - transform.position).normalized;
            Gizmos.DrawRay(transform.position, to_target);
            Vector3 cross = Vector3.Cross(transform.up, to_target);
            Gizmos.DrawRay(transform.position, cross);
        }

    }
}
