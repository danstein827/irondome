﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thrust_Control : MonoBehaviour
{
    [SerializeField] float activeDelay = 1.5f;
    Thruster thruster;
    [SerializeField] float thrust = 10f;

    // Start is called before the first frame update
    void Start()
    {
        thruster = GetComponentInChildren<Thruster>();
        thruster.Active = false;
        thruster.thrust = thrust;
        Invoke("ThrustOn", activeDelay);

    }

    void ThrustOn()
    {
        thruster.Active = true;
    }

    // Update is called once per frame
    void Update()
    {

    }

}
