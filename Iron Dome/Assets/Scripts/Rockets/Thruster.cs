﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thruster : MonoBehaviour
{

    SpriteRenderer renderer;
    GameObject parent;
    public float thrust;
    [SerializeField] bool active = false;

    public bool Active
    {
        get { return active; }
        set
        {

            renderer.enabled = value;
            active = value;
        }
    }


    // Start is called before the first frame update
    private void Start()
    {
        parent = transform.parent.gameObject;
    }


    void Awake()
    {
        renderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Active)
        {
            parent.GetComponent<Rigidbody2D>().AddForce(thrust*parent.transform.up);
        }
    }
}
