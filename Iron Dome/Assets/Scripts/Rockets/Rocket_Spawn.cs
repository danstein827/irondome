﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket_Spawn : MonoBehaviour
{
    [SerializeField] bool active = true;
    [SerializeField] float xVariation = 20;
    [SerializeField] float minDelay = 1f;
    [SerializeField] float maxDelay = 2f;
    [SerializeField] float avgDelay = 2f;
    [SerializeField] GameObject rocket;
    System.Random rnd = new System.Random();


    // Start is called before the first frame update
    void Start()
    {
        invoke_Spawn(minDelay);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void SpawnRocket()
    {
        if (active)
        {
            float rnd_x = (float)(xVariation * (rnd.NextDouble() - 0.5));
            Vector2 pos = new Vector2(rnd_x, 0);
            GameObject rocket = Instantiate(this.rocket, transform);
            rocket.transform.position = new Vector3(rnd_x, transform.position.y);
            rocket.transform.rotation = Quaternion.identity;
            float delay = (maxDelay - minDelay) * (float)rnd.NextDouble() + minDelay;
            invoke_Spawn(delay);
        }
    }

    private void invoke_Spawn(float delay)
    {
        Invoke("SpawnRocket", delay);
        Debug.Log("Next Rocket: " + delay);
    }
}
