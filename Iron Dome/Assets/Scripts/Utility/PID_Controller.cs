﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PID_Controller
{
    private float kp;
    private float ki;
    private float kd;

    private bool used = false;
    private float lastTimeCalled = 0f;
    private float integral = 0f;
    private float lastError = 0f;

    public PID_Controller(float kp, float ki, float kd)
    {
        this.kp = kp;
        this.ki = ki;
        this.kd = kd;
    }

    public float eval(float error)
    {
        float result = 0;
        float dt = Time.time - lastTimeCalled;

        if (used)
        {
            float derivative = (error - lastError) / dt;
            integral += error * dt;

            result = kp * error + ki * integral + kd * derivative;
        }
        else
        {
            integral += error * dt;
            result = kp * error;
            used = true;
        }

        lastError = error;
        lastTimeCalled = Time.time;

        return result;
    }
}
