﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_STANDALONE_WIN

public class InputManager : MonoBehaviour
{
    private GameBuild build = GameBuild.Windows;
    public GameBuild Build { get { return build; } }

    private KeyCode fire = KeyCode.Mouse0;
    private string axisX = "MouseX";
    private string axisY = "MouseY";

    public float x_Axis = 0;
    public float y_Axis = 0;
    public bool fireCommand = false;



    // Start is called before the first frame update
    void Start()
    {

        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = false;
    }



    // Update is called once per frame
    void Update()
    {
        fireCommand = Input.GetKeyDown(fire);
        x_Axis = Input.GetAxis(axisX);
        y_Axis = Input.GetAxis(axisY);


        if (Input.GetKey(KeyCode.F1))
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }
}
#endif

#if UNITY_ANDROID

public class InputManager : MonoBehaviour
{
    private GameBuild build = GameBuild.Android;
    public GameBuild Build { get { return build; } }

    private KeyCode fire = KeyCode.Mouse0;
    private string axisX = "MouseX";
    private string axisY = "MouseY";

    public float x_Axis = 0;
    public float y_Axis = 0;
    public bool fireCommand = false;



    // Start is called before the first frame update
    void Start()
    {

        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = false;
    }



    // Update is called once per frame
    void Update()
    {
        fireCommand = Input.GetKeyDown(fire);
        x_Axis = Input.GetAxis(axisX);
        y_Axis = Input.GetAxis(axisY);


        if (Input.GetKey(KeyCode.F1))
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }
}

#endif

public enum GameBuild { Windows, Android };
