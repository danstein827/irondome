﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

namespace DataLogger
{
    public class CSV_Row
    {
        private List<string> row = new List<string>();
        public List<string> GetRow() { return row; }
        public void rowAppend(List<float> values)
        {
            values.ForEach(f => { row.Add(f.ToString()); });
        }
        public void rowAppend(List<int> values)
        {
            values.ForEach(i => { row.Add(i.ToString()); });
        }
        public void rowAppend(List<string> values)
        {
            values.ForEach(s => { row.Add(s.ToString()); });
        }

        public void flush() { row = new List<string>(); }
    }

    public class CSV_writer
    {
        string title;

        public CSV_writer(string title)
        {
            this.title = title;
        }

        public void writeHeader(CSV_Row row)
        {
            StreamWriter sw = new StreamWriter(title);
            StringBuilder sb = new StringBuilder();

            row.GetRow().ForEach(s => 
            {
                sb.Append(s);
                sb.Append(',');
            });
            sb.Remove(sb.Length-1, 1);
            sw.WriteLine(sb);
            sw.Close();
            row.flush();
        }

        public void writeLine(CSV_Row row)
        {
            StreamWriter sw = new StreamWriter(title, true);
            StringBuilder sb = new StringBuilder();

            row.GetRow().ForEach(s =>
            {
                sb.Append(s);
                sb.Append(',');
            });

            sb.Remove(sb.Length - 1, 1);
            sw.WriteLine(sb);
            sw.Close();
            row.flush();
        }
    }

}