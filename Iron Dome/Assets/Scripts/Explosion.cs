﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    public float startSize = 0.2f;
    public float endSize = 1.2f;
    public float lifeTime = 2f;

    float currentLife = 0f;

    // Start is called before the first frame update
    void Start()
    {
        transform.localScale = Vector3.one * startSize;
    }

    // Update is called once per frame
    void Update()
    {
        currentLife += Time.deltaTime;
        float percentLife = currentLife / lifeTime;

        if(percentLife > 1f)
        {
            Destroy(this.gameObject);
        }

        transform.localScale = Mathf.Lerp(startSize, endSize, percentLife) * Vector3.one;

    }
}
