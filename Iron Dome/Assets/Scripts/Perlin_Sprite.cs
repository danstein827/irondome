﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Perlin_Sprite : MonoBehaviour
{
    // Width and height of the texture in pixels.
    public int pixWidth;
    public int pixHeight;

    // The origin of the sampled area in the plane.
    public bool animate = true;
    public Vector2 xOrg;
    public Vector2 yOrg;

    // The number of cycles of the basic noise pattern that are repeated
    // over the width and height of the texture.
    public float scale = 1.0F;

    [SerializeField] Gradient gradient = new Gradient();
    Color[] pix;

    public List<Texture2D> noiseTex = new List<Texture2D>(10);

    SpriteRenderer rend;

    int i = 0;


    void Start()
    {
        rend = GetComponent<SpriteRenderer>();

        // Set up the texture and a Color array to hold pixels during processing.

        pix = new Color[pixWidth * pixHeight];

        for(int i = 0; i < 10; i++)
        {
            noiseTex[i] = CalcNoise();
            
        }
    }

    Texture2D CalcNoise()
    {
        Texture2D tex = new Texture2D(pixWidth, pixHeight);

        float xCoord = Random.Range(xOrg.x, xOrg.y);
        float yCoord = Random.Range(yOrg.x, yOrg.y);

        for (int y = 0; y<pixHeight; y++)
        {
            for(int x = 0; x < pixWidth; x++)
            {
                float xP = xCoord + x * scale / pixWidth;
                float yP = yCoord + y * scale / pixHeight;
                float sample = Mathf.PerlinNoise(xP, yP);

                pix[(int)y * pixWidth + x] = gradient.Evaluate(sample);
            }
        }
        tex.SetPixels(pix);
        tex.Apply();
        return tex;
    }

    private void FixedUpdate()
    {
        
        rend.sprite = Sprite.Create(getTex(), new Rect(Vector2.zero, new Vector2(pixWidth, pixHeight)), 0.5f * Vector2.one);
    }

    private Texture2D getTex()
    {
        i++;
        if(i > noiseTex.Count) { i = 0; }
        return noiseTex[i];
    }

}
