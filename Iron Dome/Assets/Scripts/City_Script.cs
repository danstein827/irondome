﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class City_Script : MonoBehaviour
{
    List<Building> buildings;

    [SerializeField] int n_Buildings;

    static System.Random rnd = new System.Random();

    // Start is called before the first frame update
    void Start()
    {
        buildings = new List<Building> (GetComponentsInChildren<Building>());
        n_Buildings = buildings.Count;
    }

    public Building return_Target()
    {
        int b = rnd.Next(buildings.Count);
        return buildings[b];
    }

    public void destroyBuilding(Building building)
    {
        buildings.Remove(building);
    }
}
