# -*- coding: utf-8 -*-
"""
Created on Wed Feb 13 17:36:20 2019

@author: Dan-Laptop
"""

import numpy as np

class PIR:
    def __init__(self, kp = 0, ki = 0, kd = 0, u_min = -np.inf, u_max = np.inf):
        self.kp = kp
        self.ki = ki
        self.kd = kd
        self.integral = 0
        self.last_y = 0
        self.last_t = 0
        self.init = False
        
        if(u_min == -np.inf) & (u_max == np.inf):
            self.clamp_out = False
        else:
            self.clamp_out = True
            self.u_min = u_min
            self.u_max = u_max
        
    def eval(self, y_c, y, y_dot, dt):
        error = y_c - y

        self.integral += dt*error
        out = (self.kp*error 
            + self.ki*self.integral
            - self.kd*y_dot)

        if(self.clamp_out):
            out = self.clamp(out, error, dt)
            
        return out
    
    def eval_alt(self, y_c, y, time):
        error = y_c - y
        dt = time - self.last_t
        
        if(self.init):
            self.integral += dt*error
            y_dot = (y - self.last_y) / dt
        else:
            y_dot = 0
        
        out = (self.kp*error
               + self.ki*self.integral
               - self.kd*y_dot)
        
        self.last_y = y
        self.last_t = time
        self.init = True
        
        if(self.clamp_out):
            out = self.clamp(out, error, dt)
            
        return out
        
        
        
    def clamp(self, out, error, dt):
        if(out > self.u_max):
            out = self.u_max
            
            if(self.ki*error > 0):
                self.integral -= dt*error
                
        elif(out < self.u_min):
            out = self.u_min
            
            if(self.ki*error < 0):
                self.integral -= dt*error
            
        return out
    
class PID:
    def __init__(self, kp = 0, ki = 0, kd = 0, u_min = -np.inf, u_max = np.inf):
        self.kp = kp
        self.ki = ki
        self.kd = kd
        self.integral = 0
        self.last_error = 0
        self.last_t = 0
        self.init = False
        
        if(u_min == -np.inf) & (u_max == np.inf):
            self.clamp_out = False
        else:
            self.clamp_out = True
            self.u_min = u_min
            self.u_max = u_max
        
    def eval(self, y_c, y, dt):
        error = y_c - y
        
        if(self.init):
            derror = (error - self.last_error) / dt
        else:
            derror = 0
        
        self.integral += dt*error
        out = (self.kp*error 
            + self.ki*self.integral
            - self.kd*derror)
        
        self.last_error = error
        self.init = True
        
        if(self.clamp_out):
            out = self.clamp(out, error, dt)
            
        return out
    
        
    def clamp(self, out, error, dt):
        if(out > self.u_max):
            out = self.u_max
            
            if(self.ki*error > 0):
                self.integral -= dt*error
                
        elif(out < self.u_min):
            out = self.u_min
            
            if(self.ki*error < 0):
                self.integral -= dt*error
            
        return out